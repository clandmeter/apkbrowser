# apkbrowser

Alpine Linux package browser written in Python.

This package browser was initially developed in Lua (aports-turbo) but has been converted to Python by the people from postmarketOS.

Due to some missing features and changes it has now been developed as an individual project alongside the original apkbrowser. 

Some noteworthy changes:

* Make use of docker and docker compose (run app and updater seperatly)
* Make use of releases.json to automatically get the latests branches
* Re-implement automatic package flagging based on anyita (no manual flagging)
* Include Gitlab related MR to package listing
* Change some logic and naming in the form filters
* Add http retries in case of bad responses
* Cleanup some minor sql queries and add sqlite wall support back

It should be relative simple to get this project running from docker compose.
