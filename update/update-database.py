#!/usr/bin/env python3

import os
import io
import sqlite3
import configparser
import tarfile
from email.utils import parseaddr
from urllib3.util import Retry
import requests
from requests import Session
from requests.adapters import HTTPAdapter

config = configparser.ConfigParser()
config.read("config.ini")

def create_tables(db, branch):
    stmt = {}
    stmt['packages'] = {}

    flag_columns = """
            'new_version' TEXT,
            'flagged' INTEGER
    """

    flag_indexes = [
        "CREATE INDEX IF NOT EXISTS 'packages_new_version' on 'packages' (new_version)",
        "CREATE INDEX IF NOT EXISTS 'packages_flagged' on 'packages' (flagged)"
    ]

    stmt['packages']['table'] = """
        CREATE TABLE IF NOT EXISTS 'packages' (
            'id' INTEGER PRIMARY KEY,
            'name' TEXT,
            'version' TEXT,
            'description' TEXT,
            'url' TEXT,
            'license' TEXT,
            'arch' TEXT,
            'repo' TEXT,
            'checksum' TEXT,
            'size' INTEGER,
            'installed_size' INTEGER,
            'origin' TEXT,
            'maintainer' INTEGER,
            'build_time' INTEGER,
            'commit' TEXT,
            'provider_priority' INTEGER,
            'fid' INTEGER,
            'gitlab_mr' INTEGER"""

    stmt['packages']['indexes'] = [
        "CREATE INDEX IF NOT EXISTS 'packages_name' on 'packages' (name)",
        "CREATE INDEX IF NOT EXISTS 'packages_maintainer' on 'packages' (maintainer)",
        "CREATE INDEX IF NOT EXISTS 'packages_build_time' on 'packages' (build_time)",
        "CREATE INDEX IF NOT EXISTS 'packages_origin' on 'packages' (origin)",
        "CREATE INDEX IF NOT EXISTS 'packages_version' on 'packages' (version)",
        "CREATE INDEX IF NOT EXISTS 'packages_arch' on 'packages' (arch)"
    ]

    if branch == 'edge':
        stmt['packages']['table'] += ',' + flag_columns + ')'
        stmt['packages']['indexes'].extend(flag_indexes)
    else:
        stmt['packages']['table'] += ')'

    stmt['maintainer'] = {}
    stmt['maintainer']['table'] = """
        CREATE TABLE IF NOT EXISTS maintainer (
            'id' INTEGER PRIMARY KEY,
            'name' TEXT,
            'email' TEXT
        )"""

    stmt['maintainer']['indexes'] = [
        "CREATE INDEX IF NOT EXISTS 'maintainer_name' on maintainer (name)"
    ]

    stmt['repoversion'] = {}
    stmt['repoversion']['table'] = """
        CREATE TABLE IF NOT EXISTS 'repoversion' (
            'repo' TEXT,
            'arch' TEXT,
            'version' TEXT,
            PRIMARY KEY ('repo', 'arch')
        ) WITHOUT ROWID"""

    stmt['files'] = {}
    stmt['files']['table'] = """
        CREATE TABLE IF NOT EXISTS 'files' (
            'id' INTEGER PRIMARY KEY,
            'file' TEXT,
            'path' TEXT,
            'pid' INTEGER REFERENCES packages(id) ON DELETE CASCADE
        )"""

    stmt['files']['indexes'] = [
        "CREATE INDEX IF NOT EXISTS 'files_file' on 'files' (file)",
        "CREATE INDEX IF NOT EXISTS 'files_path' on 'files' (path)",
        "CREATE INDEX IF NOT EXISTS 'files_pid' on 'files' (pid)"
    ]

    for field in ["provides", "depends", "install_if"]:
        stmt[field] = {}
        stmt[field]['table'] = f"""
        CREATE TABLE IF NOT EXISTS '{field}' (
            'name' TEXT,
            'version' TEXT,
            'operator' TEXT,
            'pid' INTEGER REFERENCES packages(id) ON DELETE CASCADE
        )"""
        stmt[field]['indexes'] = [
            f"CREATE INDEX IF NOT EXISTS '{field}_name' on {field} (name)",
            f"CREATE INDEX IF NOT EXISTS '{field}_pid' on {field} (pid)"
        ]

    cur = db.cursor()
    for key in stmt:
        cur.execute(stmt[key]['table'])
        if 'indexes' in stmt[key]:
            for idx in stmt[key]['indexes']:
                cur.execute(idx)

def set_db_options(db):
    cur = db.cursor()
    cur.execute("PRAGMA journal_mode=WAL")

def get_local_repo_version(db, repo, arch):
    cur = db.cursor()
    sql = """
    SELECT version
    FROM repoversion
    WHERE repo = ?
        AND arch = ?
    """
    cur.execute(sql, [repo, arch])
    result = cur.fetchone()
    if result:
        return result[0]
    return ''


def ensure_maintainer_exists(db, maintainer):
    name, email = parseaddr(maintainer)

    if not name or not email:
        return None

    sql = """
    INSERT OR REPLACE INTO maintainer ('id', 'name', 'email')
    VALUES (
        (SELECT id FROM maintainer WHERE name=? and email=?),
        ?, ?
    ) 
    """
    cursor = db.cursor()
    cursor.execute(sql, [name, email, name, email])
    return cursor.lastrowid


def create_releases_table(db):
    sql = """
    CREATE TABLE IF NOT EXISTS 'releases' (
        'branch' TEXT,
        'repo' TEXT,
        'arch' TEXT,
        PRIMARY KEY ('branch', 'repo', 'arch')
    ) WITHOUT ROWID
    """
    db.cursor().execute(sql)
    db.cursor().execute('DELETE FROM releases')

def update_releases(db):
    cur = db.cursor()
    releases = requests.get('https://alpinelinux.org/releases.json').json()
    for idx,branch in enumerate(releases['release_branches']):
        if idx < int(config.get('repository', 'branch-history')):
            git_branch = ('edge' if branch['git_branch'] == 'master'
                          else f"v{branch['git_branch']}".split('-')[0])
            print(f'Adding branch {git_branch} to releases')
            for repo in branch['repos']:
                for arch in branch['arches']:
                    sql = """
                    INSERT OR IGNORE INTO releases (branch, repo, arch) VALUES (?, ?, ?)
                    """
                    cur.execute(sql, [git_branch, repo['name'], arch])
    db.commit()


def get_branches(db):
    sql = "SELECT DISTINCT branch FROM releases"
    return db.cursor().execute(sql).fetchall()

def get_repos(db ,branch):
    sql = "SELECT DISTINCT repo FROM releases WHERE branch = ?"
    return db.cursor().execute(sql, [branch]).fetchall()

def get_arches(db, branch, repo):
    sql = "SELECT DISTINCT arch FROM releases WHERE branch = ? AND repo = ?"
    return db.cursor().execute(sql, [branch, repo]).fetchall()


def parse_version_operator(package):
    operators = ['>=', '<=', '><', '=', '>', '<']
    for op in operators:
        if op in package:
            part = package.split(op)
            return part[0], op, part[1]
    return package, None, None


def get_file_list(url):
    print(f"Getting file list for {url}")
    response = requests.get(url)
    tar_file = io.BytesIO(response.content)
    tar = tarfile.open(fileobj=tar_file, mode='r:gz')
    result = []
    for member in tar.getmembers():
        if member.name.startswith('.'):
            continue
        if member.type == tarfile.DIRTYPE:
            continue
        result.append("/" + member.name)
    return result

def requests_retry(retries):
    http = Session()
    retries = Retry(
            total=retries,
            read=retries,
            connect=retries,
            backoff_factor=0.2,
            status_forcelist=tuple(range(401, 600)),
            allowed_methods=frozenset(['GET', 'POST']))
    http.mount('https://', HTTPAdapter(max_retries=retries))
    return http

def get_mr_from_commit(commit):
    api = config.get('gitlab', 'api')
    token = config.get('gitlab', 'token')
    retries = config.get('http', 'retries')
    project = config.get('gitlab', 'project')
    url = "{}/projects/{}/repository/commits/{}/merge_requests"
    http = requests_retry(retries)

    try:
        res = http.get(url.format(api, project, commit),
                       headers={"PRIVATE-TOKEN":token}).json()
    except Exception:
        print(f"Failed to lookup commit {commit} in gitlab API")
        return 0
    if res:
        return res[0]['iid']
    return 0

def add_packages(db, branch, repo, arch, packages):
    cur = db.cursor()
    numpkg = 1
    total = len(packages)
    for pkg in packages:
        print(f"[{numpkg}/{total}] Adding {pkg}")
        package = packages[pkg]
        if 'm' in package:
            maintainer_id = ensure_maintainer_exists(db, package['m'])
        else:
            maintainer_id = None
        package['k'] = package['k'] if 'k' in package else None

        gitlab_mr = get_mr_from_commit(package['c'])

        sql = """
        INSERT INTO 'packages' (name, version, description, url, license, arch, repo, checksum, size, installed_size,
        origin, maintainer, build_time, "commit", provider_priority, gitlab_mr)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        """
        cur.execute(sql,[
            package['P'],
            package['V'],
            package['T'],
            package['U'],
            package['L'],
            package['A'],
            repo,
            package['C'],
            package['S'],
            package['I'],
            package['o'],
            maintainer_id,
            package['t'],
            package['c'],
            package['k'],
            gitlab_mr
            ])
        pid = cur.lastrowid

        if 'p' in package:
            for provide in package['p']:
                name, operator, ver = parse_version_operator(provide)
                sql = """
                INSERT INTO provides (name, version, operator, pid) VALUES (?, ?, ?, ?)
                """
                cur.execute(sql, [name, ver, operator, pid])

        if 'i' in package:
            for iif in package['i']:
                name, operator, ver = parse_version_operator(iif)
                sql = """
                INSERT INTO install_if (name, version, operator, pid) VALUES (?, ?, ?, ?)
                """
                cur.execute(sql, [name, ver, operator, pid])

        if 'D' in package:
            for dep in package['D']:
                name, operator, ver = parse_version_operator(dep)
                sql = """
                INSERT INTO depends (name, version, operator, pid) VALUES (?, ?, ?, ?)
                """
                cur.execute(sql, [name, ver, operator, pid])

        url = config.get('repository', 'url')
        apk_url = f'{url}/{branch}/{repo}/{arch}/{package["P"]}-{package["V"]}.apk'
        files = get_file_list(apk_url)
        filerows = []
        for file in files:
            fname = os.path.basename(file)
            fpath = os.path.dirname(file)
            filerows.append([fname, fpath, pid])
        sql = """
            INSERT INTO 'files' (
                "file", "path", "pid"
            )
            VALUES (?, ?, ?)
        """
        cur.executemany(sql, filerows)
        db.commit()
        numpkg += 1

def del_packages(db, repo, arch, remove):
    cur = db.cursor()
    for package in remove:
        print(f"Removing {package}")
        part = package.split('-')
        name = '-'.join(part[:-2])
        ver = '-'.join(part[-2:])
        sql = """
        DELETE FROM packages
        WHERE repo = ?
            AND arch = ?
            AND name = ?
            AND version = ?
        """
        cur.execute(sql, [repo, arch, name, ver])
        if cur.rowcount != 1:
            print(f"Could not remove package {name} ver {ver} from {repo}/{arch}")


def clean_maintainers(db):
    sql = """DELETE FROM maintainer
        WHERE id NOT IN (SELECT maintainer FROM packages)"""
    cur = db.cursor()
    cur.execute(sql)
    print(f"{cur.rowcount} maintainers cleaned from database")

def update_local_repo_version(db, repo, arch, version):
    sql = """
    INSERT OR REPLACE INTO repoversion (
        'version', 'repo', 'arch'
    )
    VALUES (?, ?, ?)
    """
    cur = db.cursor()
    cur.execute(sql, [version, repo, arch])


def process_apkindex(db, branch, repo, arch, contents):
    dbinit = config.getboolean('database', 'init', fallback=True)
    tar_file = io.BytesIO(contents)
    tar = tarfile.open(fileobj=tar_file, mode='r:gz')
    version_file = tar.extractfile('DESCRIPTION')
    version = version_file.read().decode()
    print(version)
    if version == get_local_repo_version(db, repo, arch):
        if not dbinit:
            print(f"APKINDEX up to date for {branch}")
            return
    print("The APKINDEX on the remote server is newer, updating local repository")
    index_file = tar.extractfile('APKINDEX')
    index = io.StringIO(index_file.read().decode() + "\n")
    buffer = {}
    packages = {}
    while True:
        line = index.readline()
        line = line.strip()
        if line == '' and len(buffer) == 0:
            break
        if line == '':
            packages[buffer['P'] + '-' + buffer['V']] = buffer
            buffer = {}
        else:
            key, value = line.split(':', maxsplit=1)
            if key in "Dpi":
                # Depends, Provides and Install-if are multi-value fields
                value = value.split(' ')
            buffer[key] = value

    remote = set(packages.keys())

    sql = """
    SELECT packages.name || '-' || packages.version
    FROM packages
    WHERE repo = ?
        AND arch = ?
    """
    cur = db.cursor()
    cur.execute(sql, [repo, arch])
    local = set(map(lambda x: x[0], cur.fetchall()))

    add = remote - local
    remove = local - remote

    add_packages(db, branch, repo, arch, dict(filter(lambda arg: arg[0] in add, packages.items())))
    del_packages(db, repo, arch, remove)
    clean_maintainers(db)
    update_local_repo_version(db, repo, arch, version)


def generate():
    url = config.get('repository', 'url')
    retries = config.get('http', 'retries')

    rel_db_path = os.path.join(config.get('database', 'path'), 'releases.db')
    rel_db = sqlite3.connect(rel_db_path)
    create_releases_table(rel_db)
    update_releases(rel_db)

    for branch in get_branches(rel_db):
        db_path = os.path.join(config.get('database', 'path'), f"aports-{branch[0]}.db")
        db = sqlite3.connect(db_path)
        set_db_options(db)
        create_tables(db, branch[0])
        for repo in get_repos(rel_db, branch[0]):
            for arch in get_arches(rel_db, branch[0], repo[0]):
                apkindex_url = f'{url}/{branch[0]}/{repo[0]}/{arch[0]}/APKINDEX.tar.gz'
                http = requests_retry(retries)
                try:
                    apkindex = http.get(apkindex_url)
                except Exception:
                    print(f'skipping {arch[0]}, {apkindex_url} returned {apkindex.status_code}')
                else:
                    print(f"parsing {repo[0]}/{arch[0]} APKINDEX")
                    process_apkindex(db, branch[0], repo[0], arch[0], apkindex.content)
                    db.commit()

if __name__ == '__main__':
    generate()

