Dear {maintainer}

This is an automatic message send from pkgs.alpinelinux.org
One or more of your aports have been flagged out of date based on
Anitya monitoring system <https://release-monitoring.org/>

{aport}
To update the package you can use our helper script:

abump aport-version

If the provided information is incorrect, please let us know on IRC
or alpine-infra@alpinelinux.org. Thanks!
