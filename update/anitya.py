#!/usr/bin/env python3

import sqlite3
import math
import subprocess
import configparser
import re
from urllib3.util import Retry
import requests
from requests import Session
from requests.adapters import HTTPAdapter
from smtplib import SMTP
from email.message import EmailMessage

config = configparser.ConfigParser()
config.read("config.ini")

def requests_retry(retries):
    http = Session()
    retries = Retry(
            total=retries,
            read=retries,
            connect=retries,
            backoff_factor=0.2,
            status_forcelist=tuple(range(401, 600)),
            allowed_methods=frozenset(['GET', 'POST']))
    http.mount('https://', HTTPAdapter(max_retries=retries))
    return http

def get_local_origins(db):
    sql = """SELECT p.name, p.origin, MAX(p.version) AS version, p.repo, p.new_version,
        m.email, m.name FROM packages AS p
        LEFT JOIN maintainer AS m
        ON p.maintainer = m.id
        GROUP BY p.origin"""
    return db.cursor().execute(sql).fetchall()

def compare_versions(new, cur):
    res = subprocess.run(["apk", "version", "-t", new, cur],
                         capture_output=True, text=True)
    if res.stdout.strip() == '>':
        return True

def validate_email(email):
    if email is not None:
        regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,7}\b'
        return re.fullmatch(regex, email)

def send_mail(origin, onames):
    smtp_host = config.get('anitya', 'smtp_host')
    smtp_port = config.get('anitya', 'smtp_port')
    smtp_starttls = config.getboolean('anitya', 'smtp_starttls', fallback=True)
    local_hostname = config.get('anitya', 'local_hostname')
    smtp_timeout = int(config.get('anitya', 'smtp_timeout'))
    template = config.get('anitya', 'template')
    body = open(template, 'r').read()
    body = body.format(maintainer=origin['name'], aport=onames, version=origin['new_version'])
    msg = EmailMessage()
    msg.set_content(body)
    msg['subject'] = config.get('anitya', 'subject')
    msg['from'] = config.get('anitya', 'from')
    msg['to'] = origin['email']
    smtp = SMTP(
            host=smtp_host,
            port=smtp_port,
            local_hostname=local_hostname,
            timeout=smtp_timeout)
    if smtp_starttls:
        smtp.starttls()
    try:
        smtp.send_message(msg)
    except Exception as e:
        print(f'An error occurred: {e}')
    finally:
        smtp.quit()

def notify_maintainers(pkgs):
    mpkgs = {}
    for pkg in pkgs.values():
        if validate_email(pkg['email']):
            if pkg['email'] in mpkgs:
                mpkgs[pkg['email']].append(pkg)
            else:
                mpkgs[pkg['email']] = [pkg]
    for email, mpkg in mpkgs.items():
        ov = ""
        for aport in mpkg:
            ov = ov + f"{aport['origin']} current: {aport['version']} new: {aport['new_version']}\n"
        print('Sending email to:', email)
        send_mail(aport, ov)

def main(): 
    api = config.get('anitya', 'api')
    ipp = config.get('anitya', 'ipp')
    retries = config.get('http', 'retries')
    db_path = config.get('database', 'path')
    db = sqlite3.connect(f'{db_path}/aports-edge.db')
    db.row_factory = sqlite3.Row
    origins = get_local_origins(db)
    sql = "UPDATE packages SET new_version = ?, flagged = unixepoch() WHERE origin = ? AND repo = ?"
    cur = db.cursor()
    page = 1
    flagged = {}
    http = requests_retry(retries)
    while True:
        try:
            pkgs = http.get(f"{api}/?distribution=Alpine&page={page}&items_per_page={ipp}").json()
        except Exception:
            print(f'skipping page: {page} returned http error code: {pkgs.status_code}')
        else:
            for pkg in pkgs['items']:
                for og in origins:
                    version = og['version'].split('-r')[0]
                    if pkg['name'] == og['origin']:
                        if pkg['stable_version'] is not None:
                            if og['new_version'] != pkg['stable_version']:
                                if compare_versions(pkg['stable_version'], version):
                                    print(f"Flagging: {og['repo']}/{og['origin']} "
                                        f"Current: {version} Upstream: {pkg['stable_version']}")
                                    cur.execute(sql, [pkg['stable_version'], og['origin'], og['repo']])
                                    if og['origin'] == og['name']:
                                        flagged[og['origin']] = dict(og)
                                        flagged[og['origin']]['new_version'] = pkg['stable_version']
        if page == math.ceil(pkgs['total_items'] / pkgs['items_per_page']):
            db.commit()
            notify_maintainers(flagged)
            break
        page += 1

if __name__ == '__main__':
    if config.getboolean('anitya', 'enabled', fallback=False):
        main()
    else:
        print("Anitya integration disabled")
