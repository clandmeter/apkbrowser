import os
import sqlite3
import configparser
from math import ceil

from flask import Flask, render_template, redirect, url_for, g, request, abort

app = Flask(__name__)
application = app

config = configparser.ConfigParser()
config.read("config.ini")

def get_branches():
    db = get_db()
    sql = "SELECT DISTINCT branch FROM releases"
    return [i[0] for i in db['releases'].cursor().execute(sql).fetchall()]

def get_repos():
    db = get_db()
    sql = "SELECT DISTINCT repo FROM releases"
    return [i[0] for i in db['releases'].cursor().execute(sql).fetchall()]

def get_arches():
    db = get_db()
    sql = "SELECT DISTINCT arch FROM releases"
    return [i[0] for i in db['releases'].cursor().execute(sql).fetchall()]

def get_settings():
    return {
        "distro_name": config.get('branding', 'name'),
        "logo": config.get('branding', 'logo'),
        "favicon": config.get('branding', 'favicon'),
        "anitya": config.getboolean('anitya', 'enabled', fallback=False),
        "external": config.items('external-menu')
    }

def open_databases():
    db = {}
    dpath = config.get('database', 'path')
    db['releases'] = sqlite3.connect(os.path.join(dpath, "releases.db"))
    cur = db['releases'].cursor()
    cur.execute("SELECT DISTINCT branch FROM releases")
    for branch in [i[0] for i in cur.fetchall()]:
        db_file = os.path.join(dpath, f"aports-{branch}.db")
        db[branch] = sqlite3.connect(db_file)
        db[branch].row_factory = sqlite3.Row
    g._db = db

def get_db():
    db = getattr(g, '_db', None)
    if db is None:
        open_databases()
        db = getattr(g, '_db', None)
    return db

def get_maintainers(branch):
    db = get_db()
    cur = db[branch].cursor()
    return [i[0] for i in cur.execute("SELECT name FROM maintainer").fetchall()]

def get_filter(name, arch, repo, maintainer=None, origin=None, flagged=None, file=None, path=None):
    filter_fields = {
        "packages.name": name,
        "packages.arch": arch,
        "packages.repo": repo,
        "maintainer.name": maintainer,
        "files.file": file,
        "files.path": path
    }

    glob_fields = [
        "packages.name",
        "files.file",
        "files.path"
    ]

    where = []
    args = []
    for key in filter_fields:
        if filter_fields[key] == "" or filter_fields[key] is None:
            continue
        if key == 'packages.name' and ':' in filter_fields[key]:
            where.append("provides.name = ?")
        elif key in glob_fields:
            where.append("{} GLOB ?".format(key))
        else:
            where.append("{} = ?".format(key))
        args.append(str(filter_fields[key]))
    if origin == "yes":
        where.append("packages.origin = packages.name")
    if flagged == "yes":
        where.append("packages.flagged IS NOT NULL")
    if len(where) > 0:
        where = "WHERE " + " AND ".join(where)
    else:
        where = ""
    return where, args


def get_num_packages(branch, name=None, arch=None, repo=None, maintainer=None, origin=None, flagged=None):
    db = get_db()

    where, args = get_filter(name, arch, repo, maintainer, origin, flagged)

    pjoin = ''
    if name is not None and ':' in name:
        pjoin = 'LEFT JOIN provides ON provides.pid = packages.id'

    sql = """
    SELECT count(*) as qty
    FROM packages
    LEFT JOIN maintainer ON packages.maintainer = maintainer.id
    {}
    {}
    """.format(pjoin, where)

    cur = db[branch].cursor()
    cur.execute(sql, args)
    result = cur.fetchone()
    return result[0]


def get_packages(branch, offset, name=None, arch=None, repo=None, maintainer=None, origin=None, flagged=None):
    db = get_db()

    where, args = get_filter(name, arch, repo, maintainer, origin, flagged)

    pjoin = ''
    if name is not None and ':' in name:
        pjoin = 'LEFT JOIN provides ON provides.pid = packages.id'

    sql = """
    SELECT packages.*, datetime(packages.build_time, 'unixepoch') as build_date_time,
        maintainer.name as mname, maintainer.email as memail
    FROM packages
    LEFT JOIN maintainer ON packages.maintainer = maintainer.id
    {}
    {}
    ORDER BY packages.build_time DESC
    LIMIT 50 OFFSET ?
    """.format(pjoin, where)
    cur = db[branch].cursor()
    args.append(offset)
    return cur.execute(sql, args).fetchall()

def get_package(branch, repo, arch, name):
    db = get_db()

    sql = """
        SELECT packages.*, datetime(packages.build_time, 'unixepoch') as build_date_time,
            maintainer.name as mname, maintainer.email as memail
        FROM packages
        LEFT JOIN maintainer ON packages.maintainer = maintainer.id
        WHERE packages.repo = ? AND packages.arch = ? AND packages.name = ?
    """

    cur = db[branch].cursor()
    return cur.execute(sql, [repo, arch, name]).fetchone()

def get_num_contents(branch, name=None, arch=None, repo=None, file=None, path=None, flagged=None):
    db = get_db()

    where, args = get_filter(name, arch, repo, file=file, path=path)

    sql = """
        SELECT count(packages.id)
        FROM packages
        JOIN files ON files.pid = packages.id
        {}
    """.format(where)

    cur = db[branch].cursor()
    cur.execute(sql, args)
    result = cur.fetchone()
    return result[0]


def get_contents(branch, offset, file=None, path=None, name=None, arch=None, repo=None):
    db = get_db()

    where, args = get_filter(name, arch, repo, maintainer=None, origin=None, file=file, path=path)

    sql = """
        SELECT packages.repo, packages.arch, packages.name, files.*
        FROM packages
        JOIN files ON files.pid = packages.id
        {}
        ORDER BY files.path, files.file
        LIMIT 50 OFFSET ?
    """.format(where)

    cur = db[branch].cursor()
    args.append(offset)
    return cur.execute(sql, args).fetchall()

def get_depends_provides(branch, package_id, arch):
    db = get_db()
    sql_provides = """
        SELECT pa.repo, pa.arch, pa.name, MAX(pa.provider_priority), de.name as depname
        FROM depends de
        JOIN provides pr ON de.name = pr.name
        LEFT JOIN packages pa ON pr.pid = pa.id
        WHERE de.pid = ? AND pa.arch = ?
        GROUP BY de.name
    """
    cur = db[branch].cursor()
    provides = cur.execute(sql_provides, [package_id, arch]).fetchall()
    res = {}
    for p in provides:
        res[p['depname']] = p
    return res

def get_depends_direct(branch, package_id, arch):
    db = get_db()
    cur = db[branch].cursor()
    sql_direct = """
        SELECT dp.repo, dp.arch, dp.name, dp.provider_priority, de.name as depname
        FROM depends de
        JOIN packages dp ON dp.name = de.name
        WHERE de.pid = ? AND dp.arch = ?
    """
    direct_dependency = cur.execute(sql_direct, [package_id, arch]).fetchall()
    direct = {}
    for p in direct_dependency:
        direct[p['depname']] = p
    return direct

def get_depends(branch, package_id, arch):
    db = get_db()
    cur = db[branch].cursor()
    provides = get_depends_provides(branch, package_id, arch)
    depends_direct = get_depends_direct(branch, package_id, arch)
    sql_names = """SELECT name FROM depends WHERE pid = ?"""
    all_deps = cur.execute(sql_names, [package_id]).fetchall()

    res = []
    for dep in all_deps:
        name = dep['name']
        dep = None
        if name in depends_direct:
            dep = depends_direct[name]

        if name in provides and not dep:
            dep = provides[name]

        if dep is None:
            res.append({'name': name})
        else:
            res.append({'name': name, 'target': dep['name'], 'repo': dep['repo'], 'arch': dep['arch']})
    return res

def get_required_by(branch, package_id, arch):
    db = get_db()

    sql = """
        SELECT DISTINCT packages.* FROM provides
        LEFT JOIN depends ON provides.name = depends.name
        LEFT JOIN packages ON depends.pid = packages.id
        WHERE packages.arch = ? AND provides.pid = ?
        ORDER BY packages.name
    """

    cur = db[branch].cursor()
    return cur.execute(sql, [arch, package_id]).fetchall()

def get_subpackages(branch, repo, package_id, arch):
    db = get_db()

    sql = """
        SELECT DISTINCT packages.* FROM packages
        WHERE repo = ? AND arch = ? AND origin = ?
        ORDER BY packages.name
    """

    cur = db[branch].cursor()
    return cur.execute(sql, [repo, arch, package_id]).fetchall()

def get_install_if(branch, package_id):
    db = get_db()

    sql = """
        SELECT name, operator, version
        FROM install_if
        WHERE pid = ?
    """

    cur = db[branch].cursor()
    return cur.execute(sql, [package_id]).fetchall()

def get_provides(branch, package_id, pkgname):
    db = get_db()

    sql = """
        SELECT name, operator, version
        FROM provides
        WHERE pid = ?
            AND name != ?
    """

    cur = db[branch].cursor()
    return cur.execute(sql, [package_id, pkgname]).fetchall()

def sizeof_fmt(num, suffix='B'):
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


@app.route('/')
def index():
    return redirect(url_for("packages"))


@app.route('/packages')
def packages():
    name = request.args.get('name')
    branch = request.args.get('branch')
    repo = request.args.get('repo')
    arch = request.args.get('arch')
    maintainer = request.args.get('maintainer')
    origin = request.args.get('origin')
    flagged = request.args.get('flagged')
    page = request.args.get('page')

    form = {
        "name": name if name is not None else "",
        "branch": branch if branch is not None else config.get('repository', 'default-branch'),
        "repo": repo if repo is not None else "",
        "arch": arch if arch is not None else config.get('repository', 'default-arch', fallback=""),
        "maintainer": maintainer if maintainer is not None else "",
        "flagged": flagged if flagged is not None else "",
        "origin": origin if origin is not None else config.get('repository', 'default-origin', fallback="yes"),
        "page": int(page) if page is not None else 1
    }

    branches = get_branches()
    arches = get_arches()
    repos = get_repos()
    maintainers = get_maintainers(branch=form['branch'])

    offset = (form['page'] - 1) * 50

    packages = get_packages(
            branch=form['branch'],
            offset=offset,
            name=name,
            arch=form['arch'],
            repo=repo,
            maintainer=maintainer,
            origin=form['origin'],
            flagged=form['flagged'])

    num_packages = get_num_packages(branch=form['branch'], name=name, arch=arch, repo=repo, maintainer=maintainer,
                                    origin=origin, flagged=flagged)
    pages = ceil(num_packages / 50)

    pag_start = form['page'] - 4
    pag_stop = form['page'] + 3
    if pag_start < 0:
        pag_stop += abs(pag_start)
        pag_start = 0
    pag_stop = min(pag_stop, pages)

    return render_template("index.html",
                           **get_settings(),
                           title="Package index",
                           form=form,
                           branches=branches,
                           arches=arches,
                           repos=repos,
                           maintainers=maintainers,
                           packages=packages,
                           pag_start=pag_start,
                           pag_stop=pag_stop,
                           pages=pages)


@app.route('/contents')
def contents():
    file = request.args.get('file')
    path = request.args.get('path')
    name = request.args.get('name')
    branch = request.args.get('branch')
    repo = request.args.get('repo')
    arch = request.args.get('arch')
    page = request.args.get('page')

    form = {
        "file": file if file is not None else "",
        "path": path if path is not None else "",
        "name": name if name is not None else "",
        "branch": branch if branch is not None else config.get('repository', 'default-branch'),
        "repo": repo if repo is not None else config.get('repository', 'default-repo'),
        "arch": arch if arch is not None else "",
        "page": int(page) if page is not None else 1
    }

    branches = get_branches()
    arches = get_arches()
    repos = get_repos()

    offset = (form['page'] - 1) * 50
    if form['name'] == '' and form['file'] == '' and form['path'] == '':
        contents = []
        num_contents = 0
    else:
        contents = get_contents(branch=form['branch'], offset=offset, file=file, path=path, name=name, arch=arch,
                                repo=form['repo'])

        num_contents = get_num_contents(branch=form['branch'], file=file, path=path, name=name, arch=arch, repo=repo)

    pages = ceil(num_contents / 50)

    pag_start = form['page'] - 4
    pag_stop = form['page'] + 3
    if pag_start < 0:
        pag_stop += abs(pag_start)
        pag_start = 0
    pag_stop = min(pag_stop, pages)

    return render_template("contents.html",
                           **get_settings(),
                           title="Package index",
                           form=form,
                           branches=branches,
                           arches=arches,
                           repos=repos,
                           contents=contents,
                           pag_start=pag_start,
                           pag_stop=pag_stop,
                           pages=pages)

@app.route('/flagging')
def flagging():
    return render_template("flagging.html", **get_settings())

@app.route('/package/<branch>/<repo>/<arch>/<name>')
def package(branch, repo, arch, name):
    package = get_package(branch, repo, arch, name)

    if package is None:
        return abort(404)

    package = dict(package)

    package['size'] = sizeof_fmt(package['size'])
    package['installed_size'] = sizeof_fmt(package['installed_size'])


    git_url = config.get('external-refs', 'git-commit').format(commit=package['commit'], branch=branch, repo=repo, arch=arch,
                                                          name=name, version=package['version'],
                                                          origin=package['origin'])

    git_branch = 'master' if branch == 'edge' else '{}-stable'.format(branch[1:])

    repo_url = config.get('external-refs', 'git-repo').format(branch=git_branch, repo=repo, origin=package['origin'])

    builder = 'build-edge-{}'.format(arch) if branch == 'edge' else 'build-{}-{}'.format(branch[1:].replace('.', '-'), arch)

    build_url = config.get('external-refs', 'build-log').format(commit=package['commit'], branch=branch, repo=repo, arch=arch,
                                                           name=name, version=package['version'],
                                                           origin=package['origin'], builder=builder)

    depends = get_depends(branch, package['id'], arch)
    required_by = get_required_by(branch, package['id'], arch)
    subpackages = get_subpackages(branch, repo, package['origin'], arch)
    install_if = get_install_if(branch, package['id'])
    provides = get_provides(branch, package['id'], package['name'])

    return render_template("package.html",
                           **get_settings(),
                           title=name,
                           branch=branch,
                           git_url=git_url,
                           repo_url=repo_url,
                           build_log_url=build_url,
                           num_depends=len(depends),
                           depends=depends,
                           num_required_by=len(required_by),
                           required_by=required_by,
                           num_subpackages=len(subpackages),
                           subpackages=subpackages,
                           install_if=install_if,
                           num_install_if=len(install_if),
                           provides=provides,
                           num_provides=len(provides),
                           pkg=package)


if __name__ == '__main__':
    app.run()
